function insertLoader(){
    var tweetsDiv=$("#tweets");
	var x = $("#loaderTemplate").html();
	tweetsDiv.append($(x));	
}

function removeLoader(){
	var tweetsDiv=$("#tweets");
	tweetsDiv.html("");
}

function fetchTweets(){
	$.ajax({
	  url:'/api/tweets',
	  type:"GET",
	  dataType:'json',
	  data:{'hashtag':'custserv','count':100},
	  success:function(data){
	    var tweetsDiv=$("#tweets");
	    removeLoader();
	    var x=$("#tweetTemplate").html();
	    if(data.length>0){	    	
		    data.forEach(function(tweet){
		    	user=tweet.user;
		    	tweetTemplate=$(x);
		    	tweetTemplate.find('.user_name').html(user.name);
		    	tweetTemplate.find('.user_screen_name').html('@'+user.screen_name);
		    	tweetTemplate.find('.tweeting_user_image').attr('src',user.profile_image_url);
		    	tweetTemplate.find('.description').html(tweet.text);
		    	tweetTemplate.find('.tweet_created_at').html(moment(tweet.created_at,'dd MMM DD HH:mm:ss ZZ YYYY','en').fromNow());
		    	tweetTemplate.find('.tweet_retweet_count').append(tweet.retweet_count);
		    	tweetTemplate.find('.tweet_likes').append(tweet.favorite_count);
		    	tweetsDiv.append(tweetTemplate);
		    });
	    }
	    else{
	    	tweetsDiv.html('No tweets found.');
	    }
	  }
	});
}

$(document).ready(function(){
	insertLoader();
	fetchTweets();
});