//Library imports
var express = require('express'),
    router=express.Router(),
    config = require('./config/config');

//Initialization
var app = express();
var http=require('http').Server(app); 
require('./config/express')(app, config,router);
require('./config/routes')(app,router);

http.listen(config.port,function(){
	console.log('Listening on port: '+config.port);
});

module.exports=app;
