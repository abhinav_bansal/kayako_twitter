var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config={
  development:{
 
    port:3000,
    root:rootPath,
    twitter:{
      TOKEN_URL: 'Twitter Token URL here',
      CONSUMER_KEY: 'Twitter Customer key here',
      CONSUMER_SECRET: 'Twitter Consumer Secret here',
      BEARER_TOKEN: 'Twitter Bearer token here'
    }
  },
  test:{
    port:3000,
    root:rootPath
  },
  production:{
    port:3000,
    root:rootPath
  }
};

module.exports = config[env];
