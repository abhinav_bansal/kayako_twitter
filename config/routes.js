module.exports = function(app,router){

	// Router.use(): Mounts middleware for the routes served by the specific router.
	router.use(function(req, res, next) {
		//Middleware
		next();
	});
	
	//home route
	var home = require('../app/controllers/home');
	router.get('/',home.index);

	//API endpoint
	var twitter_api = require('../app/controllers/twitter_api/twitter');
	// API: GET request
	// Query params:
	// 	hashtag: Hashtag to search for(REQUIRED)
	// 	count: Maximum no. of tweets to get from Twitter api(DEFAULT 100) 
	// 	retweet_count: Minimum no of retweets used to filter tweets
	router.get('/api/tweets',twitter_api.getTweets);
};
