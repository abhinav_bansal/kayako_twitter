var express = require('express');

//middlewares
var favicon = require('serve-favicon');
var compress=require('compression');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var session = require('express-session');
var methodOverride = require('method-override');
var swig = require('swig');

var pkg = require('../package.json');

var env = process.env.NODE_ENV || 'development';

module.exports = function(app, config,router) {

    swig.setDefaults({
        cache: false
    });

    //Compression:  The middleware will attempt to compress response bodies for all request that traverse through the middleware.
    //Compress all requests
    app.use(compress());
    
    //Set STATIC FILES directory
    app.use(express.static(config.root + '/public'));
    
    //Set PORT NUMBER used by the application
    app.set('port', config.port);

    //Set VIEWS direcory
    app.set('views', config.root + '/app/views');
    
    // Set VIEWS templating
    //Swig: A simple, powerful, and extendable JavaScript Template Engine.
    app.engine('html',swig.renderFile);
    app.set('view engine', 'html');

    //FAVICON
    app.use(favicon(config.root + '/public/img/favicon.ico'));
    
    //Logger
    app.use(logger('dev'));
    
    // BodyParser: To add a generic JSON and URL-encoded parser as a top-level middleware, which will parse the bodies of all incoming requests.
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({extended:true}));
    // parse application/json
    app.use(bodyParser.json());

    //Method override: Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
    app.use(methodOverride());

    //NOT USED BUT KEPT FOR FUTURE REFERENCE
    //Parse Cookie header and populate req.cookies with an object keyed by the cookie names
    // CookieParser should be above session
    app.use(cookieParser());
    app.use(cookieSession({ secret: pkg.name}));

    //Use router for all paths starting at '/'(Root)
    app.use('/',router);

    //Render 404 page on Not Found(404) response code
    app.use(function(req, res) {
      res.status(404).render('404', { title: '404' });
    });




};
