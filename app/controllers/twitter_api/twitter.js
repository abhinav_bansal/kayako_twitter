// Imports
var config = require("../../../config/config");
var Twitter=require("twitter");
var utils=require("./utils");

// Get Twitter keys from config
var consumer_key=config.twitter.CONSUMER_KEY;
var consumer_secret=config.twitter.CONSUMER_SECRET;
var bearer_token=config.twitter.BEARER_TOKEN;


// Initialize Twitter client
//  --consumer_key
//  --consumer_secret
//  --bearer_token: Generated using consumer_key and consumer_secret- One time only and storecc in config.
var twitterClient = new Twitter({
	consumer_key:consumer_key,
	consumer_secret:consumer_secret,
	bearer_token:bearer_token
});

// Variables
var noHashtagError="No hashtag specified. Aborting.";
var defaultCount = 100;
var defaultMinimumRetweetCount = 1;

//Function to call Twitter API and receive tweets
exports.getTweets = function(req,res){

	// Hashtag is required, otherwise return error
	if(!req.query.hashtag){
		return res.json({"error":noHashtagError});
	}

	// Obtian hashtag to search from query params
	var hashtagToSearch = req.query.hashtag;

	// Create search query
	var searchQuery = {
		q:hashtagToSearch,
		count:parseInt(req.query.count)||defaultCount
	};

	// Set minimum retweet count for filtering
	var minimumRetweetedCount = parseInt(req.query.retweet_count)||defaultMinimumRetweetCount;

	twitterClient.get('search/tweets', searchQuery, function(error, tweets, response) {
			// console.log(tweets);
		var tweetFeed = tweets.statuses;

		// Filter based on Retweets
		var filteredTweets = tweetFeed.filter(function(tweet){
			return tweet.retweet_count>=minimumRetweetedCount;
		});

		// Return Filtered tweets
		return res.send(filteredTweets);

	});

};

