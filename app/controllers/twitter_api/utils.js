//  Imports
var config = require("../../../config/config");
var request=require("request");

// Util function to obtain BEARER_TOKEN
// Parameters: 
// 	--consumer_key: Twitter consumer key
//	--consumer_secret: Twitter consumer secret
exports.get_bearer_token = function(consumer_key,consumer_secret){
	
	//Convert consumer_key and cnsumer_secret to base64
	var key = consumer_key + ":" + consumer_secret;
	var credentials = new Buffer(key).toString('base64');

	//Setup request parameters
	var tokenUrl = config.twitter.TOKEN_URL;
	var headers={
			"Authorization":"Basic "+credentials,
			"Content-Type":"application/x-www-form-urlencoded;charset=UTF-8"
	};
	var body = "grant_type=client_credentials";
	
	//Twitter bearer token request
	request({
		url:tokenUrl,
		method:'POST',
		headers:headers,
		body: body
	},function(err,response,body){	//Callback : Token is contained in body
		if(err)
			return console.log("Error: "+err);
		
		body=JSON.parse(body);
		console.log(body.access_token);
		return body.access_token;
	});
};