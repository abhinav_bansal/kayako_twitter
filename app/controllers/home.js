exports.index = function(req, res){
  // Renders index.html
  res.render('home/index', {
    title: 'TweetFeed'
  });

};
