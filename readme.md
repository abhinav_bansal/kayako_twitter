# TweetFeed ([kayako.farzicoder.me](http://kayako.farzicoder.me))
*Challenge task for Kayako*

### Challenge
Write a simple Twitter API client in PHP. This client simply has to fetch and display Tweets that a) Have been re-Tweeted at least once and b) Contain the hashtag #custserv

### Setup and development
Clone the repository
```
git clone https://abhinav_bansal@bitbucket.org/abhinav_bansal/kayako_twitter.git
```
Navigate into the project folder and download required node dependencies
```
npm install
```
Create a twitter app and add your App CONSUMER_KEY and CONSUMER_SECRET in config/config.js. Use get_bearer_token util function to get the BEARER_TOKEN
```
node -e 'require("./app/controllers/twitter_api/utils").get_bearer_token(CONSUMER_KEY,CONSUMER_SECRET)'
```
Run app
```
gulp
```
Access application at localhost:3000

### Tech stack
* Express: Node.js web application framework
* Twitter: Client library for the Twitter REST and Streaming API's
* JQuery: Frontend javascript library for DOM manipulation and AJAX call

### Further improvements
* Adding live feed using Twitter streaming API: In 'sockets' branch. 